'use strict';
module.exports = (sequelize, DataTypes) => {
  const ExamCalifications = sequelize.define('ExamCalifications', {
    examId: DataTypes.INTEGER,
    calificationId: DataTypes.INTEGER
  }, {});
  ExamCalifications.associate = function(models) {
    // associations can be defined here
  };
  return ExamCalifications;
};