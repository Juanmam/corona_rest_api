'use strict';
module.exports = (sequelize, DataTypes) => {
  const ProfileUsers = sequelize.define('ProfileUsers', {
    profileId: DataTypes.INTEGER,
    userId: DataTypes.INTEGER
  }, {});
  ProfileUsers.associate = function(models) {
    // associations can be defined here
  };
  return ProfileUsers;
};