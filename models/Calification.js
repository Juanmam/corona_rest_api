'use strict';
module.exports = (sequelize, DataTypes) => {
  const Calification = sequelize.define('Calification', {
    calificationId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      unique: true,
      primaryKey: true
    },
    score: DataTypes.FLOAT,
    counter: { 
      type: DataTypes.INTEGER, 
      defaultValue: 1
    },
    examId: DataTypes.INTEGER,
    examName: DataTypes.STRING,
    userId: DataTypes.INTEGER,
    userName: DataTypes.STRING,
    userLastName: DataTypes.STRING
  }, {});
  classMethods: {
    Calification.associate = function(models) {
      // associations can be defined here
      Calification.belongsTo(models.user)//, { as: 'calificationsId' });
      //Calification.hasOne(models.Exam, { foreignKey: 'calificationId' });
    },{
      timestamps: true,
      paranoid: false
    };
  }
  return Calification;
};