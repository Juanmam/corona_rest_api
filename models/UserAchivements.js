'use strict';
module.exports = (sequelize, DataTypes) => {
  const UserAchivements = sequelize.define('UserAchivements', {
    userId: DataTypes.INTEGER,
    achivementId: DataTypes.INTEGER
  }, {});
  UserAchivements.associate = function(models) {
    // associations can be defined here
  };
  return UserAchivements;
};