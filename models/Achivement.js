'use strict';
module.exports = (sequelize, DataTypes) => {
  const Achivement = sequelize.define('Achivement', {
    achivementId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      unique: true,
      primaryKey: true
    },
    achivementName: DataTypes.STRING,
    description: DataTypes.TEXT,
    difficulty: DataTypes.INTEGER
  }, {});
  classMethods: {
    Achivement.associate = function(models) {
      // associations can be defined here
      Achivement.belongsToMany(models.user, { through: models.UserAchivements, foreignKey: 'achivementId' });
    },{
      timestamps: true,
      paranoid: true
    };
  }
  return Achivement;
};