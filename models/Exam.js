'use strict';
module.exports = (sequelize, DataTypes) => {
  const Exam = sequelize.define('Exam', {
    examId: {
    	type: DataTypes.INTEGER,
    	allowNull: false,
    	autoIncrement: true,
    	unique: true,
    	primaryKey: true
    },
    examName: DataTypes.STRING
  }, {});
  classMethods:{
    Exam.associate = function(models) {
      // associations can be defined here
      Exam.belongsToMany(models.Calification, { through: models.ExamCalifications, foreignKey: 'examId' });
    },{
      timestamps: true,
      paranoid: true
    };
  }
  return Exam;
};