'use strict';
module.exports = (sequelize, DataTypes) => {
  const Profile = sequelize.define('Profile', {
    profileId: {
    	type: DataTypes.INTEGER,
    	allowNull: false,
    	autoIncrement: true,
    	unique: true,
    	primaryKey: true
    },
    profileName: DataTypes.STRING
  }, {});
  classMethods:{
    Profile.associate = function(models) {
      // associations can be defined here
      Profile.belongsToMany(models.user, { through: models. ProfileUsers, foreignKey: 'profileId' });
    },{
      timestamps: true,
      paranoid: true
    };
  }
  return Profile;
};