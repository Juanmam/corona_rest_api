'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('user', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      unique: true,
      primaryKey: true
    },
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    email: DataTypes.STRING,
    name: DataTypes.STRING,
    lastName: DataTypes.STRING,
    identification: DataTypes.STRING,
    profileId: DataTypes.INTEGER,
    calificationsId: DataTypes.ARRAY(DataTypes.INTEGER),
    achivementsId: DataTypes.ARRAY(DataTypes.INTEGER)
  }, {});
  classMethods: {
    User.associate = function(models) {
      // associations can be defined here
      User.hasMany(models.Calification, { as: 'user', targetKey: 'id' });//foreignKey: 'id', targetKey: 'id'});
      User.belongsToMany(models.Achivement,
        { through: models.UserAchivements, foreignKey: 'id' });
      //User.hasOne(models.Profile, { foreignKey: 'id' });
    }, {
      timestamps: true,
      paranoid: true
    };
  }
  return User;
};