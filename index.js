const express = require('express');
const bodyParser = require('body-parser');
const models = require('./models');

//set up express app
const app = express();

app.use(bodyParser.json());

//initialize routes
console.log('Loaded routes')
app.use('/api', require('./routes/api'));

models.sequelize.sync().then(() => {
	app.set('port', (process.env.PORT || 4000));
	app.listen(app.get('port'), function(){
  		console.log('Now listening for request at port', app.get('port'));
	});
});