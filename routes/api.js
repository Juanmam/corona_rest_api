const express      = require('express');
const router       = express.Router();
const sequelize    = require('sequelize');
const models       = require('../models'); // loads index.js
const Op           = sequelize.Op;
const profile      = models.Profile;
const user         = models.user;
const exam         = models.Exam
const calification = models.Calification
const achivement   = models.Achivement

//PROFILE CRUD
//Get all
router.get('/profile/', function(req, res, next){
  profile.findAll().then(function(Profile){
    res.send({profiles: Profile});
  })
});

//Get one by id
router.get('/profile/:id', function(req, res, next){
  profile.findOne({
    where:{
      profileId: req.params.id
    }
  }).then(function(Profile){
    res.send({profile: Profile});
  })
});

//Create profile
router.post('/profile/', function(req, res, next){
  if(req.body.id){ delete req.body.id }

  profile.create(req.body).then(function(Profile){
    res.send(Profile);
  }).catch(next)
});

//Update profile
router.put('/profile/:id', function(req, res, next){
  if(req.body.id){ delete req.body.id }

  profile.update(req.body,{
    where:{
      profileId: req.params.id
    }
  }).then(function(Profile){
    res.send(req.body);
  }).catch(next)
});

//Delete profile
router.delete('/profile/:id', function(req, res, next){
  profile.destroy({
    where:{
      profileId: req.params.id
    }
  }).then(function(Profile){
    res.send({Profile_Deleted: req.params.id});
  }).catch(next)
});

//USER CRUD
//Get all
router.get('/user/', function(req, res, next){
  user.findAll({attributes: { exclude: ['password'] }}).then(function(User){
    res.send({users: User});
  })
});

//Get one by id
router.get('/user/:id', function(req, res, next){
  user.findOne({
    where:{
      id: req.params.id
    },
    attributes: { exclude: ['password'] }
  }).then(function(User){
    res.send({user: User});
  })
});

//Create user
router.post('/user/', function(req, res, next){
  if(req.body.id){ delete req.body.id }

  user.create(req.body).then(function(User){
    res.send(User);
  }).catch(next)
});

//Update user
router.put('/user/:id', function(req, res, next){
  if(req.body.id){ delete req.body.id }

  console.log(req.body)
  user.findOne({
    where:{
      id: req.params.id
    }
  }).then(function(User){
    console.log(req.body.calificationsId)
    console.log(User.calificationsId)
    if(req.body.calificationsId != null){ //No need to change
      if(User.calificationsId != null){ // check if empty
        User.calificationsId.forEach(function(element){
          req.body.calificationsId.unshift(element)
        })
      }
    }
    if(req.body.achivementsId != null){ //No need to change
      if(User.achivementsId != null){ // check if empty
        User.achivementsId.forEach(function(element){
          req.body.achivementsId.unshift(element)
        })
      }
    }
    user.update(req.body,{
      where:{
        id: req.params.id
      }
    }).then(function(User){
      res.send(req.body);
    }).catch(next)
  })
});

//Delete user
router.delete('/user/:id', function(req, res, next){
  user.destroy({
    where:{
      id: req.params.id
    }
  }).then(function(User){
    res.send({User_Deleted: req.params.id});
  }).catch(next)
});

//User Login
router.post('/user/login', function(req, res, next){
  user.findOne({
    where:{
      username: req.body.username,
      password: req.body.password
    },
    attributes: { exclude: ['password'] }
  }).then(function(User){
    if(User){
      res.send({user: User});
    } else {
      res.send("Error: No user with that name or password.")
    }
  })
});

//EXAM CRUD
//Get all
router.get('/exam/', function(req, res, next){
  exam.findAll().then(function(Exam){
    res.send({exams: Exam});
  })
});

//Get one by Id
router.get('/exam/:id', function(req, res, next){
  exam.findOne({
    where:{
      examId: req.params.id
    },
    attributes: { exclude: ['password'] }
  }).then(function(Exam){
    res.send({exam: Exam});
  })
});

//Create exam
router.post('/exam/', function(req, res, next){
  if(req.body.id){ delete req.body.id }

  exam.create(req.body).then(function(Exam){
    res.send(Exam);
  }).catch(next)
});

//Upate exam
router.put('/exam/:id', function(req, res, next){
  if(req.body.id){ delete req.body.id }

  exam.update(req.body,{
    where:{
      examId: req.params.id
    }
  }).then(function(Exam){
    res.send(req.body);
  }).catch(next)
});

//Delete exam
router.delete('/exam/:id', function(req, res, next){
  exam.destroy({
    where:{
      examId: req.params.id
    }
  }).then(function(Exam){
    res.send({Exam_Deleted: req.params.id});
  }).catch(next)
});

//CALIFICATION CRUD
//Get all
router.get('/calification/', function(req, res, next){
  calification.findAll().then(function(Calification){
    res.send({califications: Calification});
  })
});

//Get one by Id
router.get('/calification/:id', function(req, res, next){
  calification.findOne({
    where:{
      calificationId: req.params.id
    },
    attributes: { exclude: ['password'] }
  }).then(function(Calification){
    res.send({calification: Calification});
  })
});

//Create calification
router.post('/calification/', function(req, res, next){
  if(req.body.id){ delete req.body.id }

  calification.max('counter',{
    //attributes: [ sequelize.fn('MAX', sequelize.col('counter')) ],
    where:{
      [Op.or]: [{examName: req.body.examName}, {examId: req.body.examId}]
    }
  }).then(function(Calification){
    if(Calification){
      req.body.counter = Calification + 1
    } else {
      req.body.counter = 1
    }
    
    calification.create(req.body).then(function(Calification){
      res.send(Calification);
    }).catch(next)
  });
});

//Edit calification
router.put('/calification/:id', function(req, res, next){
  if(req.body.id){ delete req.body.id }
  if(req.body.counter){ delete req.body.counter }

  calification.update(req.body,{
    where:{
      calificationId: req.params.id
    }
  }).then(function(Calification){
    res.send(req.body);
  }).catch(next)
});

//Delete calification
router.delete('/calification/:id', function(req, res, next){
  calification.destroy({
    where:{
      calificationId: req.params.id
    }
  }).then(function(Calification){
    res.send({Calification_Deleted: req.params.id});
  }).catch(next)
});

//Get all califications by user id and optional exam id.
router.get('/calification/utils/:userId', function(req, res, next){
  if(req.query.examId){
    calification.findOne({
      where:{
        userId: req.params.userId,
        examId: req.query.examId
      }
    }).then(function(Calification){
      grades = Calification.dataValues.score
      //Calification.forEach(function(element){
      //  grades.push(element.dataValues.score)
      //})
      res.send({calification: grades})
    })
  } else {
    calification.findAll({
      where:{
        userId: req.params.userId
      }
    }).then(function(Calification){
      grades = []
      Calification.forEach(function(element){
        grades.push(element.dataValues.score)
      })
      res.send({calification: grades})
    })
  }
});

//ACHIVEMENT CRUD
//Get all
router.get('/achivement/', function(req, res, next){
  achivement.findAll().then(function(Achivement){
    res.send({achivements: Achivement});
  })
});

//Get on by Id
router.get('/achivement/:id', function(req, res, next){
  achivement.findOne({
    where:{
      achivementId: req.params.id
    }
  }).then(function(Achivement){
    res.send({achivement: Achivement});
  })
});

//Create achivement
router.post('/achivement/', function(req, res, next){
  if(req.body.id){ delete req.body.id }

  achivement.create(req.body).then(function(Achivement){
    res.send(Achivement);
  }).catch(next)
});

//Edit achivement
router.put('/achivement/:id', function(req, res, next){
  if(req.body.id){ delete req.body.id }

  achivement.update(req.body,{
    where:{
      achivementId: req.params.id
    }
  }).then(function(Achivement){
    res.send(req.body);
  }).catch(next)
});

//Delete achivement
router.delete('/achivement/:id', function(req, res, next){
  achivement.destroy({
    where:{
      achivementId: req.params.id
    }
  }).then(function(Achivement){
    res.send({Achivement_Deleted: req.params.id});
  }).catch(next)
});

module.exports = router;