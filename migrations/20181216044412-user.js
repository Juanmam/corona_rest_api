'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      'user',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          allowNull: false,
          autoIncrement: true,
          unique: true
        },
        username: Sequelize.STRING,
        email: Sequelize.STRING,
        name: Sequelize.STRING,
        lastName: Sequelize.STRING,
        identification: Sequelize.STRING,
        profileId: {
          type: Sequelize.INTEGER,
          references: {
            model: 'profile',
            key: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        },
        calificationsId: {
          type: Sequelize.ARRAY(Sequelize.INTEGER),
          references: {
            model: 'calification',
            key: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        },
        achivementsId: {
          type: Sequelize.ARRAY(Sequelize.INTEGER),
          references: {
            model: 'achivement',
            key: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        }, 
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE
        },
        deletedAt: {
          allowNull: false,
          type: Sequelize.DATE
        }
      }
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('user')
  }
};
