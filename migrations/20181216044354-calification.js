'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      'calification',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          allowNull: false,
          autoIncrement: true,
          unique: true
        },
        score: {
          allowNull: true,
          type: Sequelize.INTEGER
        },
        counter: Sequelize.INTEGER,
        examId: {
          type: Sequelize.INTEGER,
          references: {
            model: 'exam',
            key: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        },
        examName: Sequelize.STRING,
        userId: {
          type: Sequelize.INTEGER,
          references: {
            model: 'user',
            key: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        },
        userName: Sequelize.STRING,
        userLastName: Sequelize.STRING,
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE
        }
      }
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('calification')
  }
};
